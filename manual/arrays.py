# In this example ww going to create a simple matrix

import cv2
import numpy as np

vec = np.array([1., 2., 3.])
mat = np.array([[1., 2., 3.], [4., 5., 6.]])

#  Matriz rellena de ceros de 3 filas x 4 columnas.
#  En este caso el tipo de los elementos es float64.
M0 = np.zeros((3, 4))

#  Matriz rellena de unos de 2 filas x 3 columnas x 4 planos.
#  En este caso el tipo de los elementos es entero de 16 bits.
M1 = np.ones((2, 3, 4), dtype=np.int16)

#  Matriz con números aleatorios de 4 filas x 3 columnas.
#  En este caso el tipo de los elementos es float64
M2 = np.random.rand(4, 3)

#  Apuntar a una parte de la matriz, desde la fila 0 a la 2 (inclusive)
#  y desde la columna 1 a la 2 (inclusive)
roi = M2[0:3, 1:3]
roi[:] = -1  # asignar -1 a todos los elementos de la roi en M2

M5 = M2[1, :]  #  apunta a la parte de la matriz formada por la segunda fila.
M6 = M2[:, 2]  #  apunta a la parte de la matriz formada por la tercera columna

I24 = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]], dtype=np.uint8)

depth = np.uint8
rows = 100
cols = 200
channels = 3
black_image = np.zeros((rows, cols, channels), depth)
cv2.imshow("Black Window", black_image)
cv2.waitKey()
