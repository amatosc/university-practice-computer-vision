import cv2
import numpy as np
from matplotlib import pylab, cm, pyplot as plt

pylab.ion()

path_base = "../images/"

# figure
f = plt.figure(figsize=(20, 10))

# load image in gray grayscale and show it
I = cv2.imread(path_base + 'aerial.png', 0)
f.add_subplot(1, 2, 1)
plt.imshow(I, cmap=cm.Greys_r)

newI = cv2.equalizeHist(I)

f.add_subplot(1, 2, 2)
plt.imshow(newI, cmap=cm.Greys_r)

# end loop
pylab.show(1)
