import cv2
import numpy as np
from matplotlib import pylab, cm, pyplot as plt

pylab.ion()

path_base = "../images/"

# figure
f = plt.figure(figsize=(20, 10))

# load image in gray grayscale and show it
I = cv2.imread(path_base + 'aerial.png', 0)
f.add_subplot(1, 2, 1)
plt.imshow(I, cmap=cm.Greys_r)

# histogram config
channels = [0]
histSize = [256]
range = [0, 256]

# histogram values
hist0 = cv2.calcHist([I], channels, None, histSize, range)
minVal, maxVal, minLoc, maxLoc = cv2.minMaxLoc(hist0)
hist0 = (255 * hist0) // maxVal

# histogram image
histImage = np.zeros((256, 256), np.uint8)
for i in xrange(0, 256):
    cv2.line(histImage, (i, 255), (i, 255 - hist0[i]), 255)

f.add_subplot(1, 2, 2)
plt.imshow(histImage, cmap=cm.Greys_r)

# end loop
pylab.show(1)
