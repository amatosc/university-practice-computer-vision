import cv2
import numpy as np
from matplotlib import pylab, cm, pyplot as plt

pylab.ion()

path_base = "../images/"

# figure
f = plt.figure(figsize=(20, 10))

# load image in gray grayscale and show it
I = cv2.imread(path_base + 'aerial.png', 0)
f.add_subplot(1, 2, 1)
plt.imshow(I, cmap=cm.Greys_r)

# lineal transformation
alpha = 2
beta = 50

# slow method
# newI = np.zeros(I.shape, np.uint8)
# for i in xrange(0, I.shape[0]):
#     for j in xrange(0, I.shape[1]):
#         aux = alpha * int(I[i, j])
#         aux = aux + beta
#         if aux > 255:
#             newI[i, j] = 255
#         else:
#             newI[i, j] = aux
#
# f.add_subplot(1, 2, 2)
# plt.imshow(newI, cmap=cm.Greys_r)

# Better thant above method
# newI2 = I.astype(np.int32) * alpha + beta
# newI2 = np.clip(newI2, 0, 255)
# newI2 = newI2.astype(np.uint8)
#
# f.add_subplot(1, 2, 2)
# plt.imshow(newI2, cmap=cm.Greys_r)

# The best of all times
newI3 = cv2.add(cv2.multiply(I, alpha), beta)
f.add_subplot(1, 2, 2)
plt.imshow(newI3, cmap=cm.Greys_r)

# end loop
pylab.show(1)
