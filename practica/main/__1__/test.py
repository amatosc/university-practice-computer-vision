import os
import cv2
import unittest
import numpy as np

from practica.main.__1__ import Matcher, Trainer


class OneTestSuit(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(OneTestSuit, self).__init__(*args, **kwargs)
        self.trainer = Trainer()
        self.TESTING_PATH = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../../testing') + '/'

    def test_one(self):
        img = cv2.imread(self.TESTING_PATH + 'test31.jpg', 0)
        _m = Matcher(img, self.trainer, True)
        img = _m.draw()

        cv2.imshow("Test", img)
        cv2.waitKey()
        cv2.destroyAllWindows()

        self.assertEquals(True, True)

    def test_read_img(self):
        img = cv2.imread('/home/alex/projects/vision-artificial/practica/testing/test1.jpg', 0)

        print str(np.size(img, 1)) + 'x' + str(np.size(img, 0))
        kps, des = self.trainer.orb_process(img)
        img = cv2.drawKeypoints(img, kps)
        cv2.circle(img, self.trainer.get_center(img), 25, (0, 255, 0))
        cv2.imshow("Test", img)
        cv2.waitKey()
        cv2.destroyAllWindows()
        self.assertEquals(True, True)


if __name__ == '__main__':
    unittest.main()
