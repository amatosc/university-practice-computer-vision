import numpy as np
from numpy import unravel_index

import cv2
import os

from practica.progressbar import progressbar


class Information:
    def __init__(self, des=None, kp=None, center=None):
        self.des = des
        self.kp = kp
        self.angle = np.arctan2(np.linalg.norm(np.cross(center, kp.pt)), np.dot(center, kp.pt))
        self.size = kp.size
        self.v_to_center = np.subtract(center, kp.pt)


class Trainer:
    PATH_TRAINING = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../../training/cars') + '/'
    N_FEATURES = 150  # should be 100
    N_LEVELS = 6  # should be 4
    SCALE_FACTOR = 1.3  # should be 1.3
    FLANN_INDEX_LSH = 6

    def __init__(self, path=None):
        if path is None:
            self.path = self.PATH_TRAINING
        else:
            self.path = path

        print ('Initializing trainer..')
        print('Reading files and init obr process...')
        print self.PATH_TRAINING
        files = os.listdir(self.PATH_TRAINING)
        index_params = dict(algorithm=self.FLANN_INDEX_LSH, table_number=12, key_size=20, multi_probe_level=2)
        search_params = dict(checks=500)

        self._progress = progressbar.AnimatedProgressBar(width=100, end=len(files))
        self._orb = cv2.ORB(nfeatures=self.N_FEATURES, nlevels=self.N_LEVELS, scaleFactor=self.SCALE_FACTOR)
        self._matcher = cv2.FlannBasedMatcher(index_params, search_params)
        self.info = dict()
        self._position = 0

        self._progress.show_progress()
        for idx, name_file in enumerate(files):
            image = self.read_image(name=name_file)
            self._process_image(image)
            self._progress + 1
            self._progress.show_progress()
        print
        self._matcher.train()

        print('Training finished')

    def _process_image(self, image):
        center = self.get_center(image)
        kps, des = self.orb_process(image=image)
        for idx, kp in enumerate(kps):
            info = Information(des=des[idx], kp=kp, center=center)
            self.info.setdefault(self._position, info)
            self._position += 1

        self._matcher.add([des])

    def read_image(self, name):
        return cv2.imread(self.PATH_TRAINING + name, cv2.CV_LOAD_IMAGE_GRAYSCALE)

    def orb_process(self, image):
        _kp, _des = self._orb.detectAndCompute(image, None)
        return tuple([_kp, _des])

    def matcher(self, query, k=6):
        return self._matcher.knnMatch(query, k)

    def get_info_by_index(self, index):
        return self.info.get(index)

    @staticmethod
    def get_size(image):
        w = np.size(image, 1)
        h = np.size(image, 0)
        return {'w': w, 'h': h}

    @staticmethod
    def get_center(image):
        size = Trainer.get_size(image)
        return tuple([size['w'] // 2, size['h'] // 2])


class Matcher:
    _PADDING = 300

    def __init__(self, image=None, trainer=None, debug=False):
        size = Trainer.get_size(image)
        kps, des = trainer.orb_process(image)

        self.votes = []
        self._image = image
        self._trainer = trainer
        self._init_voting(size)

        for idx, kp in enumerate(kps):
            self._process(des[idx], kp)

        self._center = unravel_index(self.votes.argmax(), self.votes.shape)
        self._center = map(lambda x: x * 10, self._center)

        if debug:
            print(Trainer.get_center(self._image))
            self._image = cv2.drawKeypoints(self._image, kps)
            cv2.circle(self._image, tuple(Trainer.get_center(self._image)), 10, (0, 0, 255))
            cv2.circle(self._image, tuple(self._center), 15, (0, 255, 0))

    def draw(self):
        return self._image

    def _process(self, des, kp):
        matches = self._trainer.matcher(des)

        for match in matches:
            for dMach in match:
                info = self._trainer.get_info_by_index(dMach.trainIdx)
                coord = tuple([info.v_to_center[0] + kp.pt[0], info.v_to_center[1] + kp.pt[1]])
                self._vote(coord, info.size / kp.size)

    def _init_voting(self, size):
        h = 1 if size['h'] < 10 else size['h'] // 10
        w = 1 if size['w'] < 10 else size['w'] // 10
        self.votes = np.zeros([h, w])

    def _vote(self, coord, size=1):
        coord = map(lambda c: c // 10, coord)
        if coord[0] < 0:
            coord[0] = 0

        if coord[1] < 0:
            coord[1] = 0

        if coord[0] >= len(self.votes):
            coord[0] = len(self.votes) - 1

        if coord[1] >= len(self.votes[0]):
            coord[1] = len(self.votes[0]) - 1

        self.votes[coord[0]][coord[1]] += 1 * size

    def get_center(self):
        return self._center

    def has_center(self):
        return self._center[0] != 0 or self._center[1] != 0
