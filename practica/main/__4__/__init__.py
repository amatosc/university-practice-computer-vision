import numpy as np
import os

import cv2

from practica.main.__1__ import Trainer
from practica.main.__2__ import CascadeClassifier


class LocaleCarId:
    def __init__(self):
        self._init_cascade()
        self.trainer = Trainer()
        self.car_cascade = CascadeClassifier()

    def _init_cascade(self, xml_file=None):
        if xml_file is None:
            self.DEFAULT_FILE = os.path.abspath(
                os.path.dirname(os.path.abspath(__file__)) + '/../../haars') + '/matriculas.xml'
        else:
            self.DEFAULT_FILE = xml_file

        self._cascade = cv2.CascadeClassifier(self.DEFAULT_FILE)

    def detect(self, img):
        return self._cascade.detectMultiScale(img)

    def detect_filtered(self, img, debug=False):
        card_zones, all = self.car_cascade.detect_filtered(img, self.trainer)

        if debug:
            img = self.draw_roi(card_zones, img, (255, 0, 0))

        if len(card_zones) == 0:
            id_zones = self.detect(img)
        else:
            for (y, x, h, w) in card_zones:
                img_z = img[x:x + w, y:y + h]
                id_zones = self.detect(img_z)
                id_zones = [(yz + y, xz + x, hz, wz) for (yz, xz, hz, wz) in id_zones]

        if debug:
            img = self.draw_roi(id_zones, img)

        return id_zones, img

    def read(self, img, detect=False, zones=None):
        ths = []
        if not detect:
            zones, i = self.detect_filtered(img)

            if len(zones) == 0:
                zones = self.detect(img)

            if len(zones) > 1:
                zones = [self._clean(zones)]
        else:
            zones = zones

        bounds = []
        for idx, (y, x, h, w) in enumerate(zones):
            img_th = img[x:x + w, y:y + h]
            img_th = cv2.medianBlur(img_th, 5)
            img_th = cv2.adaptiveThreshold(img_th, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
            contours, hierarchy = cv2.findContours(img_th, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

            for points in contours:
                xp, yp, wp, hp = cv2.boundingRect(points)
                bounds.append([xp + y, yp + x, wp, hp])

            cv2.drawContours(img_th, contours, -1, (255, 255, 0))
            img[x:x + w, y:y + h] = img_th

            ths.append((y, x, h, w))

        return ths, img, bounds

    @staticmethod
    def draw_roi(zones, img, color=(0, 255, 0)):
        if np.size(img[1, 1]) == 1:
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

        if len(zones) > 0:
            for (x, y, w, h) in zones:
                cv2.rectangle(img, (x, y), (x + w, y + h), color, 1)

        return img

    def _clean(self, zones):
        m_out, x_out, y_out, w_out, h_out = np.infty, np.infty, np.infty, 0, 0

        # for (x, y, w, h) in zones:
        #     p1 = (x, y)
        #     p2 = (x + w, y + h)
        #     m = np.sqrt((np.power(p1, 2) + np.power(p2, 2)))
        #     if m_out == np.infty or m < m_out:
        #         x_out, y_out, w_out, h_out = (x, y, w, h)

        for (x, y, w, h) in zones:
            if x < x_out:
                x_out = x

            if y < y_out:
                y_out = y

            if w > w_out:
                w_out = w

            if h > h_out:
                h_out = h

        return x_out, y_out, w_out, h_out
