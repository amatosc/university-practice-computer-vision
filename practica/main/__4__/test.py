import unittest

import cv2
import os

from practica.main.__4__ import LocaleCarId


class LocaleCardIdTestCase(unittest.TestCase):
    TESTING_PATH = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../../testing/ocr') + '/'

    def test_image_1(self):
        l = LocaleCarId()
        img = cv2.imread(self.TESTING_PATH + 'frontal_1.jpg', 0)
        zones = l.detect(img)

        img = l.draw_roi(zones, img)

        cv2.imshow("Test", img)
        cv2.waitKey()
        cv2.destroyAllWindows()
        self.assertEqual(True, True)

    def test_image_filtered(self):
        l = LocaleCarId()
        img = cv2.imread(self.TESTING_PATH + 'frontal_10.jpg', 0)
        zones, img = l.detect_filtered(img, True)

        cv2.imshow("Test", img)
        cv2.waitKey()
        cv2.destroyAllWindows()
        self.assertEqual(True, True)

    def test_read_one(self):
        l = LocaleCarId()
        img = cv2.imread(self.TESTING_PATH + 'frontal_25.jpg', 0)
        zones, img, bounds = l.read(img)
        img = l.draw_roi(bounds, img)

        cv2.imshow("Test", img)
        cv2.waitKey()
        cv2.destroyAllWindows()
        self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()
