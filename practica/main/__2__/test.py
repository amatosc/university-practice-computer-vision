import os
import unittest

import cv2

from practica.main.__1__ import Trainer
from practica.main.__2__ import CascadeClassifier


class TwoTestCase(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TwoTestCase, self).__init__(*args, **kwargs)
        self.cascade = CascadeClassifier()
        self.TESTING_PATH = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../../testing') + '/'

    def test_one(self):
        img = cv2.imread(self.TESTING_PATH + 'test2.jpg', 0)
        zones = self.cascade.detect(img)
        img = self.cascade.draw_roi(zones, img)

        cv2.imshow("Test", img)
        cv2.waitKey()
        cv2.destroyAllWindows()

        self.assertEquals(True, True)

    def test_two(self):
        img = cv2.imread(self.TESTING_PATH + 'test2.jpg', 0)
        zones, all = self.cascade.detect_filtered(img, Trainer())
        img = self.cascade.draw_roi(zones, img, all)

        cv2.imshow("Test", img)
        cv2.waitKey()
        cv2.destroyAllWindows()

        self.assertEquals(True, True)

if __name__ == '__main__':
    unittest.main()
