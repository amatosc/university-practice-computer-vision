import numpy as np
import os

import cv2

from practica.main.__1__ import Matcher


class CascadeClassifier:
    def __init__(self, xml_file=None):
        self._init_cascade(xml_file)

    def _init_cascade(self, xml_file=None):
        if xml_file is None:
            self._DEFAULT_FILE = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../../haars') + '/coches.xml'
        else:
            self._DEFAULT_FILE = xml_file

        self._cascade = cv2.CascadeClassifier(self._DEFAULT_FILE)

    def detect(self, img):
        return self._cascade.detectMultiScale(img)

    def detect_filtered(self, img, trainer):
        zones = self.detect(img)
        out = tuple([list(), list()])
        for (y, x, h, w) in zones:
            img_z = img[x:x + w, y:y + h]
            m = Matcher(img_z, trainer)
            if m.has_center():
                out[0].append([y, x, h, w])

            out[1].append([y, x, h, w])

        return out

    @staticmethod
    def draw_roi(zones, img, all=None):
        if np.size(img[1, 1]) == 1:
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

        if all is not None and len(all) > 0:
            for (x, y, w, h) in all:
                cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 1)

        if len(zones) > 0:
            for (x, y, w, h) in zones:
                cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)

        return img
