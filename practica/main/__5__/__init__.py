import os
import cv2
import numpy as np

from sklearn.lda import LDA
from practica.progressbar import progressbar


def get_key(chars):
    if len(chars) == 1:
        return ord(chars)
    else:
        s = 0
        for c in chars:
            s += get_key(c)

        return s


class OCRTrainer:
    ESP_CONST = 1000

    def __init__(self):
        self.path = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../../training/ocr') + '/'

        print ('Initializing ocr trainer..')
        print('Reading files...')
        files = os.listdir(self.path)

        self.tags = []
        self.features = []
        self._progress = progressbar.AnimatedProgressBar(width=100, end=len(files))
        self._progress.show_progress()

        for idx, name_file in enumerate(files):
            image = self.read_image(name_file)
            img_th, bounds, v = self._process_image(image)

            if name_file.split('_')[0] == 'ESP':
                self.tags.append(self.ESP_CONST)
            else:
                self.tags.append(ord(name_file.split('_')[0]))

            self.features.append(v)
            self._progress + 1
            self._progress.show_progress()
        print

        print('LDA process...')
        self.lda = LDA()
        self.lda.fit(self.features, self.tags)
        self.features = self.lda.transform(self.features)

        print('SVM process..')
        self.svg_model = cv2.SVM()
        self.svg_model.train(np.asarray(self.features, np.float32), np.asarray(self.tags, np.float32))

        print('Training finished')

    def query(self, sample):
        th, bounds, v = self._process_image(sample)
        v = self.lda.transform(v)
        v = np.asanyarray(v[0], np.float32)
        out = self.svg_model.predict(v)
        if int(out) == self.ESP_CONST:
            return 'ESP'
        else:
            return chr(int(out))

    def _process_image(self, img):
        img = cv2.resize(img, (10, 10))
        th = cv2.medianBlur(img, 5)
        th = cv2.adaptiveThreshold(th, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
        v = np.asarray(th).reshape(-1)
        contours, hierarchy = cv2.findContours(th, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
        bounds = []
        for point in contours:
            bounds.append(cv2.boundingRect(point))

        return th, bounds, v

    def read_image(self, name):
        return cv2.imread(self.path + name, cv2.CV_LOAD_IMAGE_GRAYSCALE)
