import cv2

from practica.main.__1__ import Trainer
from practica.main.__2__ import CascadeClassifier


class VideoMatch:
    def __init__(self, path=None):
        if path is None:
            raise TypeError('Path must be defined')

        self._trainer = Trainer()
        self._cap = cv2.VideoCapture(path)
        self._cascade = CascadeClassifier()

        while self._cap.isOpened():
            ret, frame = self._cap.read()

            if frame is None:
                break

            if not ret:
                continue

            img = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            zones, all = self._cascade.detect_filtered(img, self._trainer)

            img = self._cascade.draw_roi(zones, img, all)
            cv2.imshow('frame', img)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        self._cap.release()
        cv2.destroyAllWindows()
