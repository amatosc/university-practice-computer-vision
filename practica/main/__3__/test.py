import os
import unittest

from practica.main.__3__ import VideoMatch


class ThreeTestCase(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(ThreeTestCase, self).__init__(*args, **kwargs)
        self.TESTING_PATH = os.path.abspath(os.path.dirname(os.path.abspath(__file__)) + '/../../testing/videos') + '/'

    def test_one(self):
        VideoMatch(self.TESTING_PATH + 'video2.wmv')
        self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()
