import os
import sys

import cv2

from practica.main.__4__ import LocaleCarId
from practica.main.__5__ import OCRTrainer


class Info(object):
    def __init__(self, image_name, x, y, car_id, width):
        self.image_name = image_name
        self.x, self.y = x, y
        self.car_id = car_id
        self.width = width

    def __str__(self):
        return '<' + self.image_name + '>' + '<' + str(self.x) + '>' '<' + str(self.y) + '>' '<' + self.car_id + '>' '<' + str(self.width) + '>\n'


if __name__ == '__main__':
    base_path, path = sys.argv
    list_dir = os.listdir(path)
    lci = LocaleCarId()
    ocr = OCRTrainer()

    out = []
    for file in list_dir:
        img_o = cv2.imread(os.path.join(path, file), cv2.CV_LOAD_IMAGE_GRAYSCALE)
        zone, img, bounds = lci.read(img_o)
        car_id = ''

        if len(zone) == 0:
            out.append(Info(file, 0, 0, '', 0))
            continue

        for (y, x, h, w) in bounds:
            car_id += ocr.query(img_o[x:x + w, y:y + h])

        out.append(Info(file, zone[0][1], zone[0][0], car_id, zone[0][3] / 2))

    fo = open(os.path.join(os.path.dirname(base_path), 'testing_ocr.txt'), 'wb')

    for i in out:
        fo.write(i.__str__())
